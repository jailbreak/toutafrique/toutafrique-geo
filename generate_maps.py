#!/usr/bin/env python3
"""
Generate africa and world GeoJSON or TopoJSON maps from Natural earth data project.

See http://www.naturalearthdata.com/
Using pycountry to get country id.
"""
import argparse
import csv
import logging
import sys
import tempfile
from enum import Enum
from pathlib import Path
from typing import Dict

import geopandas as gpd
import topojson
import ujson as json


class GeoFormat(Enum):
    """Geographical JSON formats."""

    GEO_JSON = "geojson"
    TOPO_JSON = "topojson"

    def __str__(self):
        """Convert to string."""
        return self.value


# Properties to retain
PROPERTY_LIST = ["ADM0_A3", "NAME_FR", "geometry"]

# Properties renaming
RENAMED_PROPERTY_LIST = ["code", "name", "geometry"]

# Country code mapping
COUNTRY_CODE_MAPPING = {
    "PSX": "PSE",  # Palestine
    "SAH": "ESH",  # Western sahara
    "SDS": "SSD",  # South sudan
}

TARGET_PROJECTION = "EPSG:4326"

log = logging.getLogger(__name__)


def dump_geo_dataframe(gdf: gpd.GeoDataFrame, geo_format: GeoFormat, out_file: Path):
    """Dump GeoDataFrame to file in GeoJSON or TopoJSON format."""
    # TopoJSON output
    if geo_format == GeoFormat.TOPO_JSON:
        tj = topojson.Topology(gdf)
        tj.to_json(fp=out_file, pretty=True, indent=2)
        return

    # else GeoJSON (default) output

    # First GeoJSON geopandas default serialization
    with tempfile.NamedTemporaryFile(delete=False, mode="wb") as fd:
        gdf.to_file(fd, driver="GeoJSON")
        temp_file = Path(fd.name)
    with temp_file.open("rt", encoding="utf-8") as fdin:
        geojson_dict = json.load(fdin)
    temp_file.unlink()

    # Add feature ids
    for cp, feature in enumerate(geojson_dict.get("features")):
        feature_prop = feature.get("properties")
        if feature_prop is None:
            continue
        feature["id"] = cp + 1

    # More compact JSON serialization
    with out_file.open("wt", encoding="utf-8") as fdout:
        json.dump(geojson_dict, fdout, sort_keys=True, ensure_ascii=False)


def change_feature_code_and_name(
    gdf: gpd.GeoDataFrame,
    feature_code: str,
    new_feature_code: str,
    new_feature_name: str,
):
    """Change feature code and name of a feature found by its code."""
    gdf.loc[:, "name"] = [
        new_feature_name if code == feature_code else name
        for code, name in zip(gdf["code"].tolist(), gdf["name"].tolist())
    ]
    gdf.loc[:, "code"] = [
        new_feature_code if code == feature_code else code
        for code in gdf["code"].tolist()
    ]


def prepare_world_map(ne_source_shp_file: Path):
    """Factor code."""
    # Load main file
    countries_gdf = gpd.read_file(ne_source_shp_file)
    countries_gdf = countries_gdf.to_crs(TARGET_PROJECTION)

    # World map
    world_gdf = countries_gdf.loc[:, PROPERTY_LIST]
    world_gdf.columns = RENAMED_PROPERTY_LIST
    world_gdf["code"] = [
        # Fix some codes (South Sudan, Western Sahara...)
        COUNTRY_CODE_MAPPING.get(code, code)
        for code in world_gdf["code"].tolist()
    ]

    # Remove Antartica as in R original app
    world_gdf = world_gdf[~world_gdf["code"].isin(["ATA"])]

    # Somaliland is merged with Somalia
    change_feature_code_and_name(world_gdf, "SOL", "SOM", "Somalie")
    world_gdf = world_gdf.dissolve(by="code", as_index=False)

    return world_gdf


def generate_map_files(
    ne_source_shp_file: Path,
    shiny_source_shp_file: Path,
    geo_format: GeoFormat,
    target_dir: Path,
    african_country_info_map: Dict[str, Dict[str, str]],
):
    """Generate GeoJSON or TopoJSON files from SHP file."""
    # Remove Morocco and Western Sahara
    world_gdf = prepare_world_map(ne_source_shp_file)
    world_gdf = world_gdf[~world_gdf["code"].isin(["MAR", "ESH"])]

    # Use Morocco and Western Sahara from the other shapefile
    shiny_gdf = gpd.read_file(shiny_source_shp_file)
    shiny_gdf = shiny_gdf.to_crs(TARGET_PROJECTION)
    shiny_gdf = shiny_gdf.loc[:, ["ISO_A3", "NAME", "geometry"]]

    shiny_gdf = shiny_gdf.loc[shiny_gdf.ISO_A3.isin(["MAR", "ESH"])]
    shiny_gdf.columns = ["code", "name", "geometry"]
    shiny_gdf.loc[shiny_gdf.code == "MAR", "name"] = "Maroc"
    shiny_gdf.loc[shiny_gdf.code == "ESH", "name"] = "Sahara occidental"

    world_gdf = world_gdf.append(shiny_gdf)

    # Add special property for Western Sahara
    world_gdf["crosshatched"] = [
        True if code == "ESH" else False for code in world_gdf["code"]
    ]

    # Western Sahara is a new Morocco
    # But we don't merge both features
    # to have a chance to style Western Sahara independently of Morocco
    change_feature_code_and_name(world_gdf, "ESH", "MAR", "Maroc")

    # map file extension
    ext = geo_format.value

    dump_geo_dataframe(world_gdf, geo_format, target_dir / f"world.{ext}")

    # Africa export
    africa_gdf = world_gdf[world_gdf["code"].isin(african_country_info_map.keys())]
    dump_geo_dataframe(africa_gdf, geo_format, target_dir / f"africa.{ext}")

    # Africa "Direction régionale" export
    # We start again from World map without modifying Morocco and Western Sahara
    # boundings to insure nice area merge
    world_gdf = prepare_world_map(ne_source_shp_file)
    africa_gdf = world_gdf[world_gdf["code"].isin(african_country_info_map.keys())]

    africa_dr_gdf = africa_gdf.loc[:, RENAMED_PROPERTY_LIST]

    def code_to_dr_name(row, african_info_map):
        return african_info_map.get(row.code)["dr_name"]

    africa_dr_gdf["name"] = africa_dr_gdf.apply(
        code_to_dr_name, args=(african_country_info_map,), axis=1
    )

    africa_dr_gdf["code"] = africa_dr_gdf["code"].map(
        lambda code: african_country_info_map[code]["dr_code"]
    )
    africa_dr_gdf = africa_dr_gdf.dissolve(by="code")
    dump_geo_dataframe(africa_dr_gdf, geo_format, target_dir / f"africa_dr.{ext}")


def main():
    """Generate World and Africa TopoJSON files."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "source_dir", type=Path, help="where to read source data",
    )
    parser.add_argument(
        "country_info_csv_file",
        type=Path,
        help="african country and direction regionale info file",
    )
    parser.add_argument(
        "target_dir", type=Path, help="where to generate TopoJSON files"
    )
    parser.add_argument(
        "--topojson",
        action="store_true",
        help="use topojson format instead of geojson (default)",
    )
    args = parser.parse_args()

    geo_format = GeoFormat.TOPO_JSON if args.topojson else GeoFormat.GEO_JSON

    ne_source_shp_file = Path(
        args.source_dir / "ne_110m_admin_0_countries/ne_110m_admin_0_countries.shp"
    )
    shiny_source_shp_file = Path(args.source_dir / "shiny_monde/MONDE.shp")

    if not args.country_info_csv_file.exists():
        parser.error(f"CSV file not found: {args.country_info_csv_file}")
    country_info_csv_file = args.country_info_csv_file

    with country_info_csv_file.open("rt", encoding="utf-8") as fdin:
        reader = csv.DictReader(fdin)
        african_country_info_map = {row["country_code"]: row for row in reader}

    if not args.target_dir.exists():
        parser.error(f"Target directory not found: {args.target_dir}")
    target_dir = args.target_dir

    generate_map_files(
        ne_source_shp_file,
        shiny_source_shp_file,
        geo_format,
        target_dir,
        african_country_info_map,
    )


if __name__ == "__main__":
    sys.exit(main())
