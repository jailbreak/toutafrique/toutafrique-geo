# Tout Afrique Geo

Python scripts that generate normalized GeoJSON or TopoJSON files from:

- [Natural earth data project](http://www.naturalearthdata.com/)
- Original R-app shapefile for Morocco and Western Sahara areas

## Source data

Source data is shipped with this repository in `src_data` directory, which contains:

- `ne_110m_admin_0_countries`: Natural earth data, cf below
- `shiny_monde`: comes from the previous version of the application developed in R with Shiny

### Natural earth data download

This is optional, do it only if you need to update data.

Criteria to choose right Natural earth release:

- small scale data (1:110m): keep maps light
- cultural: no need to have physical information

From [download page](https://www.naturalearthdata.com/downloads/110m-cultural-vectors/):

- in section "Admin 0 - Countries", click on "Download countries" button
- the downloaded file should be named `ne_110m_admin_0_countries.zip`

```bash
mkdir -p src_data/ne_110m_admin_0_countries
mv ne_110m_admin_0_countries.zip src_data/ne_110m_admin_0_countries
cd src_data/ne_110m_admin_0_countries
unzip ne_110m_admin_0_countries.zip
rm ne_110m_admin_0_countries.zip
```

## GeoJSON and TopoJSON generation

```bash
mkdir dist
```

GeoJSON generation (`--format` parameter can be omitted)

```bash
python3 generate_maps.py --format geojson src_data <path/to/african_countries_info.csv> dist
```

TopoJSON

```bash
python3 generate_maps.py --format topojson src_data <path/to/african_countries_info.csv> dist
```

Note: `african_countries_info.csv` file can be found in [toutafrique-data-processing](https://framagit.org/jailbreak/toutafrique/toutafrique-data-processing/)
project.

### Output

3 maps are written:

- `world.(geo|topo)json`: world countries map
- `africa.(geo|topo)json`: african countries map
- `africa_dr.(geo|topo)json`: african `direction régionale` map

Notes:

- `Western Sahara` - `Morocco` border is not correct in world map provided by Natural Earth project
  - for these 2 areas, world and african map use the outlines from R-app shapefile
  - for `africa_dr.topojson`, original outlines are used while they are melted into `North Africa` area
- `africa_countries.topojson` is a subset of `world_countries.topojson` restricted to
  african countries
- geometry contains one feature per country:
  - `id`: country numeric id
  - properties:
    - `code`: ISO-3166 alpha_3 code
    - `name`: country name (french)

## Deployment

The 3 output files can be copied to the same S3 Bucket than the one described in [data-processing](https://framagit.org/jailbreak/toutafrique/toutafrique-data-processing/) so that the [Web API](https://framagit.org/jailbreak/toutafrique/toutafrique-api/) serves them.
